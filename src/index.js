import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import collect from 'collect.js'

function Square (props) {
  return (
    <button
      className={props.class}
      onClick={props.onClick}
    >
      {props.value}
    </button>
  )
}

class Board extends React.Component {
  renderSquare (i) {
    return <Square
      class={this.props.winninglines.includes(i) ? 'square congrats' : 'square'}
      key={i}
      value={this.props.squares[i]}
      onClick={() => this.props.onClick(i)}
    />
  }

  render () {
    const items = []
    collect([...Array(16).keys()]).chunk(4).toArray().forEach((value, index) => {
      let row = []
      value.forEach((square) => {
        row.push(this.renderSquare(square))
      })
      items.push(
        <div key={index} className="board-row">
          {row}
        </div>
      )
    })

    return (<div>{items}</div>)
  }
}

class Game extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      history: [{ squares: Array(9).fill(null), location: null }],
      stepNumber: 0,
      xIsNext: true,
      stepsSortAsc: true,
    }
  }

  handleClick (i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1),
      current = history[history.length - 1],
      squares = current.squares.slice(),
      winLines = calculateWinner(squares)

    if (winLines.winner || squares[i]) {
      return
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O'
    this.setState({
      history: history.concat([
        {
          squares: squares,
          location: {
            col: (i % 4 + 1),
            row: (Math.floor(i / 4) + 1)
          }
        },
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    })
  }

  jumpTo (step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
    })
  }

  toggleSort () {
    this.setState({
      stepsSortAsc: !this.state.stepsSortAsc,
    })
  }

  render () {
    const history = this.state.history,
      current = history[this.state.stepNumber],
      winner = calculateWinner(current.squares)

    let moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move + ' @ (' + step.location.col + ', ' + step.location.row + ')' :
        'Go to game start'
      return (
        <li key={move}>
          <button
            className={(this.state.stepNumber !== move) ? 'btn-jump' : 'btn-jump bold-move'}
            onClick={() => this.jumpTo(move)}>
            {desc}
          </button>
          {(this.state.stepNumber === move) ? ' ◄' : ''}
        </li>
      )
    })
    if (!this.state.stepsSortAsc) {
      moves = moves.reverse()
    }

    let status
    if (!winner.winner && this.state.stepNumber === 9) {
      status = 'Game drawn'
    } else {
      if (winner.winner) {
        status = 'Winner: ' + winner.winner
      } else {
        status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O')
      }
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            winninglines={winner.line}
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div className="status-block">{status}</div>
          &nbsp;
          <Sorter
            icon={this.state.stepsSortAsc ? '▲' : '▼'}
            onClick={() => this.toggleSort()}
          />
          <ul>{moves}</ul>
        </div>
      </div>
    )
  }
}

function Sorter (props) {
  return (<button
    className="btn-jump btn-click"
    onClick={props.onClick}
  >
    {props.icon}
  </button>)
}

// ========================================

ReactDOM.render(<Game/>, document.getElementById('root'))

function calculateWinner (squares) {
  const lines = [
    [0, 1, 2, 3],
    [4, 5, 6, 7],
    [8, 9, 10, 11],
    [12, 13, 14, 15],
    [0, 4, 8, 12],
    [1, 5, 9, 13],
    [2, 6, 10, 14],
    [3, 7, 11, 15],
    [0, 5, 10, 15],
    [3, 6, 9, 12],
  ]
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c, d] = lines[i]
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c] && squares[a] === squares[d]) {
      return { winner: squares[a], line: lines[i] }
    }
  }
  return { winner: null, line: [] }
}
